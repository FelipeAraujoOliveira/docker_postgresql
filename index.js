const express = require('express');
const { Client } = require('pg');

const app = express();
const port = 3000;

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'basegeografica',
  password: '1234',
  port: 5432,
});

client.connect()
  .then(() => console.log('Conectado ao banco de dados'))
  .catch(err => console.error('Erro ao conectar ao banco de dados', err));


app.get('/', async (req, res) => {
  try {
    
    const queryRegioes = 'SELECT * FROM "Regioes"';
    const resultRegioes = await client.query(queryRegioes);

    const queryEstados = 'SELECT * FROM "Estados"';
    const resultEstados = await client.query(queryEstados);

    res.render('index', {
      regioes: resultRegioes.rows,
      estados: resultEstados.rows
    });
  } catch (error) {
    console.error('Erro ao buscar registros:', error);
    res.status(500).send('Erro ao buscar registros');
  }
});

app.set('view engine', 'ejs');

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
